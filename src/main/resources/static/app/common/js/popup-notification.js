(function () {
    const toastIsShow = document.querySelector('.toast-placement-ex');
    if (toastIsShow !== null) {
        setTimeout(function () {
            toastIsShow.classList.remove('hide');
            toastIsShow.classList.add('show');
        }, 1000);
        setTimeout(function () {
            toastIsShow.classList.remove('show');
            toastIsShow.classList.add('hide');
        }, 6000);
    }
})();
