CREATE TABLE "m_permission" (
                                "id" VARCHAR(50) NOT NULL,
                                "code" VARCHAR(50) NOT NULL,
                                "name" VARCHAR(50) NOT NULL,
                                "created_date" TIMESTAMP NULL DEFAULT NULL,
                                "created_by" VARCHAR(50) NULL DEFAULT NULL,
                                "updated_date" TIMESTAMP NULL DEFAULT NULL,
                                "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                                PRIMARY KEY ("id")
)
;

CREATE TABLE "m_role" (
                          "id" VARCHAR(50) NOT NULL,
                          "name" VARCHAR(50) NOT NULL,
                          "code" VARCHAR(5) NOT NULL,
                          "created_date" TIMESTAMP NULL DEFAULT NULL,
                          "created_by" VARCHAR(50) NULL DEFAULT NULL,
                          "updated_date" TIMESTAMP NULL DEFAULT NULL,
                          "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                          PRIMARY KEY ("id")
)
;

CREATE TABLE "m_role_permission" (
                                     "id" VARCHAR(50) NOT NULL,
                                     "role_id" VARCHAR(50) NOT NULL,
                                     "permission_id" VARCHAR(50) NOT NULL,
                                     PRIMARY KEY ("id"),
                                     CONSTRAINT "m_role_permission_permission_id_fkey" FOREIGN KEY ("permission_id") REFERENCES "public"."m_permission" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION,
                                     CONSTRAINT "m_role_permission_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "public"."m_role" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
)
;

CREATE TABLE "m_user" (
                          "id" VARCHAR(50) NOT NULL,
                          "username" VARCHAR(50) NOT NULL,
                          "name" VARCHAR(100) NOT NULL,
                          "birth_of_date" DATE NULL DEFAULT NULL,
                          "place_of_birth" VARCHAR(50) NULL DEFAULT NULL,
                          "gender" CHAR(1) NULL DEFAULT NULL,
                          "address" VARCHAR NULL DEFAULT NULL,
                          "occupation" VARCHAR(50) NULL DEFAULT NULL,
                          "password" VARCHAR NOT NULL,
                          "role_id" VARCHAR(50) NULL DEFAULT NULL,
                          "active" BOOLEAN NOT NULL DEFAULT 'true',
                          "created_date" TIMESTAMP NULL DEFAULT NULL,
                          "created_by" VARCHAR(50) NULL DEFAULT NULL,
                          "updated_date" TIMESTAMP NULL DEFAULT NULL,
                          "updated_by" VARCHAR(50) NULL DEFAULT NULL,
                          PRIMARY KEY ("id"),
                          CONSTRAINT "m_user_role_id_fkey" FOREIGN KEY ("role_id") REFERENCES "public"."m_role" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION
)
;