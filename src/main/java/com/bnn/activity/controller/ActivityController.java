package com.bnn.activity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/activity")
public class ActivityController {
    @GetMapping("/test")
    public String getActivity() {
        return "activity/test";
    }
    @GetMapping("/dokumentation")
    public String getDokumentation() {
        return "activity/dokumentation";
    }
}
