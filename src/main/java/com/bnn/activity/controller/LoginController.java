package com.bnn.activity.controller;

import com.bnn.activity.dto.request.SignupRequest;
import com.bnn.activity.dto.response.SignupResponse;
import com.bnn.activity.form.SignupForm;
import com.bnn.activity.helper.AuthenticationHelper;
import com.bnn.activity.service.MUserService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class LoginController {

    private final AuthenticationHelper authenticationHelper;
    private final MUserService mUserService;

    public LoginController(AuthenticationHelper authenticationHelper, MUserService mUserService) {
        this.authenticationHelper = authenticationHelper;
        this.mUserService = mUserService;
    }

    @ModelAttribute
    public SignupForm setupSignupForm() {
        return new SignupForm();
    }

    @GetMapping("/login")
    public String getLogin() {
        boolean isAuthenticated = this.authenticationHelper.isAuthenticated();
        if (isAuthenticated) {
            return "redirect:/";
        } else {
            return "welcome/login";
        }
    }

    @GetMapping("/signup")
    public String getSignUp() {
        boolean isAuthenticated = this.authenticationHelper.isAuthenticated();
        if (isAuthenticated) {
            return "redirect:/";
        } else {
            return "welcome/signup";
        }
    }

    @PostMapping("/signup")
    public String submitSignUp(@Validated @ModelAttribute SignupForm signupForm){
        SignupRequest request = new SignupRequest(signupForm);
        SignupResponse response = this.mUserService.insertUser(request);
        return "redirect:/login";
    }
}
