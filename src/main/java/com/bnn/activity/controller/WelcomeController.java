package com.bnn.activity.controller;

import com.bnn.activity.helper.AuthenticationHelper;
import com.bnn.activity.service.MUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
@RequestMapping
public class WelcomeController {

    private final MUserService mUserService;
    private final AuthenticationHelper authenticationHelper;

    public WelcomeController(MUserService mUserService, AuthenticationHelper authenticationHelper) {
        this.mUserService = mUserService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/")
    public String dashboard(HttpServletRequest request) {
        Locale currentLocal = request.getLocale();
        String countryCode = currentLocal.getCountry();
        String countryName = currentLocal.getDisplayName();
        String langCode = currentLocal.getLanguage();
        String langName = currentLocal.getDisplayLanguage();
        return "dashboard/index";
    }

}
