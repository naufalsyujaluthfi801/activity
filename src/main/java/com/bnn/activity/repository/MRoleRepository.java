package com.bnn.activity.repository;

import com.bnn.activity.model.MRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MRoleRepository extends JpaRepository <MRole, String> {
}
