package com.bnn.activity.repository;

import com.bnn.activity.model.MPermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MPermissionRepository extends JpaRepository <MPermission, String> {
}
