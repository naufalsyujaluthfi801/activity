package com.bnn.activity.repository;

import com.bnn.activity.model.MRolePermission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MRolePermissionRepository extends JpaRepository <MRolePermission, String>{
}
