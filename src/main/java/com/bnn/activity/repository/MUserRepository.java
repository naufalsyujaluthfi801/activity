package com.bnn.activity.repository;

import com.bnn.activity.model.MUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MUserRepository extends JpaRepository<MUser, String> {

    @Query("select t from MUser t where t.username =:username")
    MUser findMUserByUsername(String username);

    boolean existsMUserByUsername(String username);

    @Query("select concat('ROLE_', p.code) from MUser u inner join MRolePermission rp on rp.role = u.role inner join MPermission p on p.id = rp.permission.id where u.username =:username")
    List<String> findAllPermissionCodeByUsername(String username);

}
