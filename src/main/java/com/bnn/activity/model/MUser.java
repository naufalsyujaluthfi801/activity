package com.bnn.activity.model;

import com.bnn.activity.dto.request.SignupRequest;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@Table(name = "m_user")
@Entity
public class MUser implements Serializable {
    @Id
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @Column(name = "username", nullable = false, length = 50)
    private String username;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "birth_of_date")
    private Date birthOfDate;

    @Column(name = "place_of_birth", length = 50)
    private String placeOfBirth;

    @Column(name = "gender", length = 1)
    private String gender;

    @Column(name = "address")
    private String address;

    @Column(name = "occupation", length = 50)
    private String occupation;

    @Column(name = "password", nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private MRole role;

    @Column(name = "active", nullable = false)
    private Boolean active = true;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "created_by", length = 50)
    private String createdBy;

    @Column(name = "updated_date")
    private Date updatedDate;

    @Column(name = "updated_by", length = 50)
    private String updatedBy;

    public MUser() {
    }

    public MUser(SignupRequest request, String password) {
        this.id = UUID.randomUUID().toString();
        this.username = request.getUsername();
        this.name = request.getName();
        this.password = password;
        this.active = true;
        this.createdDate = new Date();
        this.createdBy = id;
    }
}