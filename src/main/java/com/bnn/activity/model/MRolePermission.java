package com.bnn.activity.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "m_role_permission")
@Entity
public class MRolePermission {
    @Id
    @Column(name = "id", nullable = false, length = 50)
    private String id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    private MRole role;

    @ManyToOne(optional = false)
    @JoinColumn(name = "permission_id", nullable = false)
    private MPermission permission;

    public MRolePermission() {
    }

}