package com.bnn.activity.form;

import com.bnn.activity.constant.MessageConstant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginForm {
    @NotEmpty(message = MessageConstant.VALIDATION_MESSAGE_0002)
    private String username;
    @NotEmpty(message = MessageConstant.VALIDATION_MESSAGE_0004)
    private String password;
}
