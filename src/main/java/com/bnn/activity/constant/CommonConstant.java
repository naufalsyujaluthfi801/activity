package com.bnn.activity.constant;

public class CommonConstant {
    public static final int FAILED_RESPONSE_CODE = 0;
    public static final int SUCCESS_RESPONSE_CODE = 1;

    public static final String FAILED_RESPONSE_MESSAGE = "Failed";
    public static final String SUCCESS_RESPONSE_MESSAGE = "Success";

    public static final String REGISTER_ACTION_CODE = "Register";
    public static final String UPDATE_ACTION_CODE = "Update";
    public static final String DELETE_ACTION_CODE = "Delete";

    public static final boolean FALSE_MESSAGE = false;
    public static final boolean TRUE_MESSAGE = true;

    public static final String LOGIN_MESSAGE = "Login";
    public static final String LOGOUT_MESSAGE = "Logout";

    public static final String EXCEPTION = "EXCEPTION: ";
}
