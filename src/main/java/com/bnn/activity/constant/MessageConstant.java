package com.bnn.activity.constant;

public class MessageConstant {
    /** {0} registered successful. */
    public static final String COMMON_MESSAGE_0001 = "common.message.0001";
    /** {0} modified successful. */
    public static final String COMMON_MESSAGE_0002 = "common.message.0002";
    /** {0} deleted successful. */
    public static final String COMMON_MESSAGE_0003 = "common.message.0003";
    /** {0} registered unsuccessful. */
    public static final String COMMON_MESSAGE_0004 = "common.message.0004";
    /** {0} modified unsuccessful. */
    public static final String COMMON_MESSAGE_0005 = "common.message.0005";
    /** {0} deleted unsuccessful. */
    public static final String COMMON_MESSAGE_0006 = "common.message.0006";

    public static final String ERROR_MESSAGE_0001 = "error.message.0001";
    public static final String ERROR_MESSAGE_0002 = "error.message.0002";
    public static final String ERROR_MESSAGE_0003 = "error.message.0003";
    public static final String ERROR_MESSAGE_0004 = "error.message.0004";
    public static final String ERROR_MESSAGE_0005 = "error.message.0005";
    public static final String ERROR_MESSAGE_0006 = "error.message.0006";
    public static final String ERROR_MESSAGE_0007 = "error.message.0007";
    public static final String ERROR_MESSAGE_0008 = "error.message.0008";
    public static final String ERROR_MESSAGE_0009 = "error.message.0009";

    public static final String VALIDATION_MESSAGE_0001 = "{validation.message.0001}";
    public static final String VALIDATION_MESSAGE_0002 = "{validation.message.0002}";
    public static final String VALIDATION_MESSAGE_0003 = "{validation.message.0003}";
    public static final String VALIDATION_MESSAGE_0004 = "{validation.message.0004}";
    public static final String VALIDATION_MESSAGE_0005 = "{validation.message.0005}";
    public static final String VALIDATION_MESSAGE_0006 = "{validation.message.0006}";
    public static final String VALIDATION_MESSAGE_0007 = "{validation.message.0007}";
    public static final String VALIDATION_MESSAGE_0008 = "{validation.message.0008}";
    public static final String VALIDATION_MESSAGE_0009 = "{validation.message.0009}";
    public static final String VALIDATION_MESSAGE_0010 = "{validation.message.0010}";
    public static final String VALIDATION_MESSAGE_0011 = "{validation.message.0011}";
    public static final String VALIDATION_MESSAGE_0012 = "{validation.message.0012}";
    public static final String VALIDATION_MESSAGE_0013 = "{validation.message.0013}";

    /** Yes */
    public static final String COMMON_PAGE_0012 = "common.page.0012";
    /** No */
    public static final String COMMON_PAGE_0013 = "common.page.0013";
    /** Male */
    public static final String COMMON_PAGE_0014 = "common.page.0014";
    /** Female */
    public static final String COMMON_PAGE_0015 = "common.page.0015";


    public static final String USER_PAGE_0001 = "user.page.0001";

}
