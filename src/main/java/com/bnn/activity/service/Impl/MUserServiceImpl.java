package com.bnn.activity.service.Impl;

import com.bnn.activity.constant.CommonConstant;
import com.bnn.activity.constant.MessageConstant;
import com.bnn.activity.dto.request.SignupRequest;
import com.bnn.activity.dto.response.SignupResponse;
import com.bnn.activity.model.MUser;
import com.bnn.activity.repository.MUserRepository;
import com.bnn.activity.service.MUserService;
import com.bnn.activity.utils.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
public class MUserServiceImpl implements MUserService {

    private MUserRepository mUserRepository;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public MUserServiceImpl(MUserRepository mUserRepository, BCryptPasswordEncoder passwordEncoder) {
        this.mUserRepository = mUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public SignupResponse insertUser(SignupRequest request) {
        SignupResponse response = new SignupResponse();
        try {
            boolean isExists = this.mUserRepository.existsMUserByUsername(request.getUsername());
            if (!isExists) {
                String password = passwordEncoder.encode(request.getPassword()).toString();
                MUser mUser = new MUser(request, password);
                this.mUserRepository.save(mUser);
                response.setIsSuccess(true);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0001, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
            } else {
                response.setIsSuccess(false);
                response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
            }
        } catch (Exception e) {
            log.error(CommonConstant.EXCEPTION, e);
            response.setIsSuccess(false);
            response.setMessage(MessageUtils.getMessageString(MessageConstant.COMMON_MESSAGE_0004, MessageUtils.getMessageString(MessageConstant.USER_PAGE_0001)));
        }
        return response;
    }

}
