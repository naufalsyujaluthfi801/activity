package com.bnn.activity.service;

import com.bnn.activity.dto.request.SignupRequest;
import com.bnn.activity.dto.response.SignupResponse;

public interface MUserService {
    SignupResponse insertUser(SignupRequest request);
}
