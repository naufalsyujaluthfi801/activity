package com.bnn.activity.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralResponse {
    private String message;
    private Boolean isSuccess;
    private int totalPages;
    private long totalItems;
}
