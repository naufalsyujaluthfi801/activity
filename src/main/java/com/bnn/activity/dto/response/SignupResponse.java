package com.bnn.activity.dto.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class SignupResponse extends GeneralResponse{
}
