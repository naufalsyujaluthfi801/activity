package com.bnn.activity.dto.request;

import com.bnn.activity.form.LoginForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
    private String username;
    private String password;

    public LoginRequest(LoginForm form) {
        this.username = form.getUsername();
        this.password = form.getPassword();
    }
}
