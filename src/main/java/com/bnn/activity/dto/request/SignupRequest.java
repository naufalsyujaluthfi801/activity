package com.bnn.activity.dto.request;

import com.bnn.activity.form.SignupForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignupRequest {
    private String name;
    private String username;
    private String password;

    public SignupRequest(SignupForm form) {
        this.name = form.getName();
        this.username = form.getUsername();
        this.password = form.getPassword();
    }
}

